import React from 'react'
import { Flipped } from 'react-flip-toolkit'
import { Customer } from '../../generated/graphql'
import { makeStyles, Theme, createStyles } from '@material-ui/core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUserCircle } from '@fortawesome/free-solid-svg-icons'
import Typography from '@material-ui/core/Typography'

export const useCustomerGridCardStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      width: '100%',
      height: '100%',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      flexDirection: 'column',
      padding: '1rem',
      '&:hover': {
        cursor: 'pointer',
      },
    },
    title: {
      margin: '1rem 0',
    },
  }),
)

interface CustomerGridCardProps {
  customer: Customer
}

const CustomerGridCard = ({ customer }: CustomerGridCardProps) => {
  const classes = useCustomerGridCardStyles()
  return (
    <Flipped flipId={customer.id} stagger>
      <Flipped inverseFlipId={customer.id.toString()}>
        <div className={classes.container}>
          <FontAwesomeIcon icon={faUserCircle} size="4x" />
          <Flipped flipId={`${customer.id}-content`} delayUntil={customer.id} translate>
            <>
              <Typography className={classes.title} variant="h6" noWrap>
                {customer.firstname} {customer.lastname}
              </Typography>
              <Typography variant="body1" noWrap>
                {customer.email}
              </Typography>
            </>
          </Flipped>
        </div>
      </Flipped>
    </Flipped>
  )
}

export default CustomerGridCard
