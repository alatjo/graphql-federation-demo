import React from 'react'
import { Skeleton } from '@material-ui/lab'
import { Grid, Paper } from '@material-ui/core'
import { useCustomerGridStyles } from './CustomerGrid'

export default function CustomerGridSkeleton() {
  const classes = useCustomerGridStyles()
  return (
    <Grid container className={classes.root}>
      <Grid container justify="center" spacing={4}>
        {[1, 2, 3, 4, 5].map((key) => (
          <Paper key={key} className={classes.paper} style={{ margin: '1rem', padding: '1rem' }}>
            <Skeleton animation="wave" variant="circle" width={60} height={60} style={{ margin: '1rem auto 0' }} />
            <Skeleton animation="wave" height="80%" />
          </Paper>
        ))}
      </Grid>
    </Grid>
  )
}
