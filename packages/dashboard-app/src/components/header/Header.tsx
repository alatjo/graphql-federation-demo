import React from 'react'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import useHeaderStyles from './HeaderStyles'

export default function Header() {
  const classes = useHeaderStyles()

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <Typography className={classes.title} variant="h6" noWrap>
            Dashboard App
          </Typography>
        </Toolbar>
      </AppBar>
    </div>
  )
}
