import { ApolloServer } from "apollo-server-lambda";
import { DataSources } from "apollo-server-core/dist/requestPipeline";

import resolvers from "./resolvers";
import CustomerAPI from "../customer.api";
import { buildFederatedSchema } from "@apollo/federation";
import { APIs } from "@shared/types";

const typeDefs = require("../schema.graphql");

const server = new ApolloServer({
  schema: buildFederatedSchema([{ typeDefs, resolvers: resolvers as any }]),
  dataSources: (): DataSources<APIs> => ({
    CustomerAPI: new CustomerAPI(
      process.env.CUSTOMER_REST_API_HOST,
      process.env.CUSTOMER_REST_API_PORT,
      "customers"
    ),
  }),
});

exports.graphqlHandler = server.createHandler();
