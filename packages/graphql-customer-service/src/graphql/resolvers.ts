import {
  customerResolver,
  customerByIdResolver,
} from "../customer.resolver";

export default {
  Query: {
    customers: customerResolver,
    customer: customerByIdResolver,
  },
};
