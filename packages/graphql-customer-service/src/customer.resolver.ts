import { Customer, QueryResolvers } from "@shared/generated/graphql";
import { ResolverContext } from "@shared/types";

const customerResolver: QueryResolvers<ResolverContext>["customers"] = async (
  _,
  __,
  { dataSources }
): Promise<Customer[]> => {
  return await dataSources.CustomerAPI.getCustomers();
};

const customerByIdResolver: QueryResolvers<ResolverContext>["customer"] = async (
  _,
  { customerId },
  { dataSources }
): Promise<Customer> => {
  return await dataSources.CustomerAPI.getCustomerById(customerId!);
};

export { customerResolver, customerByIdResolver };
