import CommentAPI from "../../graphql-comment-service/src/comment.api";
import CustomerAPI from "../../graphql-customer-service/src/customer.api";

export interface APIs {
  CustomerAPI: CustomerAPI;
  CommentAPI: CommentAPI;
}

export interface ResolverContext {
  dataSources: APIs;
}
