import { ApolloServer, SchemaDirectiveVisitor } from "apollo-server-lambda";
import { DataSources } from "apollo-server-core/dist/requestPipeline";

import resolvers from "./resolvers";
import { APIs } from "@shared/types";
import CommentAPI from "../comment.api";
import { buildFederatedSchema } from "@apollo/federation";
import directives from "../directives";

const typeDefs = require("../schema.graphql");
const schema = buildFederatedSchema([
  { typeDefs, resolvers: resolvers as any },
]);
SchemaDirectiveVisitor.visitSchemaDirectives(schema, directives);

const server = new ApolloServer({
  schema,
  dataSources: (): DataSources<APIs> => ({
    CommentAPI: new CommentAPI(
      process.env.COMMENT_REST_API_HOST,
      process.env.COMMENT_REST_API_PORT,
      "comments"
    ),
  }),
});

exports.graphqlHandler = server.createHandler();
