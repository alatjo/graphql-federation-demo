import { commentResolver, commentsByEmailResolver } from "../comment.resolver";

export default {
  Customer: {
    comments: commentResolver,
  },
  Query: {
    comments: commentsByEmailResolver,
  },
};
