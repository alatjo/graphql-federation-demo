import { SchemaDirectiveVisitor } from "apollo-server-lambda";
import { GraphQLField, defaultFieldResolver } from "graphql";
import { format as dateFormat } from "date-fns";

export default class DateFormatterDirective extends SchemaDirectiveVisitor {
  visitFieldDefinition(field: GraphQLField<any, any>) {
    const { resolve = defaultFieldResolver } = field;
    const { format } = this.args;
    field.resolve = async function (...args) {
      const dateIsoString = await resolve.apply(this, args);
      if (dateIsoString) {
        const formattedDate = dateFormat(new Date(dateIsoString), format);
        return formattedDate;
      }
      return null;
    };
  }
}
