import DateFormatterDirective from "./DateFormatterDirective";

export default {
  dateFormat: DateFormatterDirective,
};
