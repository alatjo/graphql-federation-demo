import { Comment } from "@shared/generated/graphql";

export interface ICommentAPI {
  getMessagesByUserId(userId: string): Promise<Comment[]>;
}
