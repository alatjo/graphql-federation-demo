import {
  Comment,
  CustomerResolvers,
  QueryResolvers,
} from "@shared/generated/graphql";
import { ResolverContext } from "@shared/types";

const commentResolver: CustomerResolvers<ResolverContext>["comments"] = async (
  { id, email },
  _args,
  { dataSources }
): Promise<Comment[]> => {
  if (!email) {
    console.warn("no email field set on Customer with id: ", id);
    return [];
  }
  return await dataSources.CommentAPI.getMessagesByUserId(email);
};

const commentsByEmailResolver: QueryResolvers<ResolverContext>["comments"] = async (
  _,
  { email },
  { dataSources }
): Promise<Comment[]> => {
  return await dataSources.CommentAPI.getMessagesByUserId(email);
};

export { commentResolver, commentsByEmailResolver };
