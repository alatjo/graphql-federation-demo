import { ApolloGateway } from "@apollo/gateway";
import { ApolloServer } from "apollo-server";

const port = 8080;

const gateway = new ApolloGateway({
  serviceList: [
    {
      name: "customer-service",
      url: "http://localhost:8081/local/graphql",
    },
    {
      name: "comment-service",
      url: "http://localhost:8082/local/graphql",
    },
  ],
});

const server = new ApolloServer({
  gateway,
  subscriptions: false,
});

server.listen({ port }).then(({ url }) => {
  console.log(`Federation Gateway running in ${url}`);
});
