# GraphQL Federation Demo

Based on [GraphQL API Integration Demo](https://bitbucket.org/alatjo/graphql-api-integration-demo/src/master/). Migrate one existing Grahql intergation service into a Federation Gateway and two underlaying services (Customer & Comment service).

## Prerequisities

- serverless (https://serverless.com/)
- node v.14 (https://nodejs.org/en/)
- yarn (https://yarnpkg.com/)

## Development

Install dependencies:

- `yarn install`

Start all backend services:

- `yarn start:be`

Start frontend:

- `yarn start:fe //from project root or`
- `yarn start //from packages/dashboard-app`


### GraphQL Federation API

- Test queries: _test/queries_

#### Federation Gateway

- Playground: http://localhost:8080/graphql

#### Customer Service

- Playground: http://localhost:8081/local/graphql

#### Comment Service

- Playground: http://localhost:8082/local/graphql

### Customer API

- endpoint: http://localhost:3000/customers
- mock data: _rest-customer-api/customer.json_

### Comments API

- endpoint: http://localhost:4000/comments
- mock data: _rest-comment-api/comments.json_

### Dashboard App

- UI: http://localhost:3001/
